<?php
include 'Mobile_Detect.php';
$detect = new Mobile_Detect;
#print_r($detect);
if ( $detect->isMobile() ) {
  header('location:https://m.chiantigeografico.it');
  exit;
}
$galleries = [
  'san_gimignano' => '#sectionAnchor2/3',
  'gaiole_in_chianti' => '#sectionAnchor2/4',
  'sangiovese' => 'Gallery Sangiovese',
  'vini_enoteche' => 'Gallery Vini ed Enoteche'
];

$meta = [
        'description' => 'Questa è una storia che parla di vino, di territorio, di identità e di
solidarietà. Questa è la storia del Chianti Geografico.
“C’erano una volta diciassette uomini, tutti agricoltori e strenui difensori
della propria terra. Nel 1961, fondarono la “Cooperativa fra agricoltori del
Chianti Geografico”. Poco dopo la sua fondazio
ne, tale cooperativa divenne
uno dei più importanti punti di riferimento della produzione di questo
rinomato vino toscano: il Chianti, appunto.'
];

$spsl_default = [
  'gallery' => '#sectionAnchor2/3',
  'wine_cards' => '#sectionAnchor3/1',
  'wine_shop' => 'https://www.chiantigeografico.it/wineshop',
  'reserve_tasting' => '#sectionAnchor3/2',
];

$social_links = [
  'facebook' => 'https://www.facebook.com/agricoltori.geografico/',
  'instagram' => 'https://www.instagram.com/geografico_wines/?hl=it',
  'youtube' => 'https://www.youtube.com/channel/UCUJuvvubHHagyFhBkd2wXxQ'
];

$sections = [
  [
    'title' => 'Home',
    'subtitle' => 'Scopri il chianti<br>Geografico',
    'index' => 0,
    'background' => 'PicciniVigneto.png',
  ],
  [
    'title' => 'Storia',
    'subtitle' => 'Un sogno lungo<br>58 anni',
    'index' => 1,
    'background' => '1-storia.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '',
        'extraClasses' => '',
        'title' => 'Video'
      ],
      [
        'index' => 1,
        'background' => 'timelineSlide',
        'extraClasses' => 'timelineSlide',
        'title' => 'Timeline'
      ],
      [
        'index' => 2,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'Storia',
        'has_common_gallery' => true
      ],
      [
        'index' => 3,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100',
        'title' => 'Il territorio',
        'has_common_gallery' => true
      ],
      [
        'index' => 4,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100',
        'title' => 'Mission & Vision',
        'has_common_gallery' => true
      ],
      [
        'index' => 5,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100',
        'title' => 'Mappa Storica'
      ]
    ]
  ],
  [
    'title' => 'Cantine',
    'subtitle' => 'San Gimignano<br>& Gaiole',
    'index' => 2,
    'background' => '2-le-nostre-cantine.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '2-le-nostre-cantine.jpg',
        'extraClasses' => 'd-flex h-100 backgroundOverlay',
        'title' => 'Le Nostre Cantine'
      ],
      [
        'index' => 1,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'San Gimignano',
        'has_common_gallery' => true
      ],
      [
        'index' => 2,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'Gaiole in Chianti',
        'has_common_gallery' => true
      ],
      [
        'index' => 3,
        'background' => 'bg-std-slides.png',
        'extraClasses' => 'galleryPage d-flex h-100',
        'title' => 'Galleria'
      ]
      /*,[
      'index' => 4,
      'background' => 'bg-std-slides.png',
      'extraClasses' => 'galleryPage d-flex h-100',
      'title' => 'Galleria Gaiole in Chianti'
      ]*/
    ]

  ],
  [
    'title' => 'Vini & Enoteche',
    'subtitle' => 'Gallery Wine<br>& Shop',
    'index' => 3,
    'background' => '3-vini-e-enoteche.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '3-vini-e-enoteche.jpg',
        'extraClasses' => 'd-flex h-100 backgroundOverlay justify-content-center',
        'title' => 'Vini & Enoteche'
      ],
      [
        'index' => 1,
        'background' => '',
        'extraClasses' => 'productPage d-flex h-100 ',
        'title' => 'Schede vini'
      ],
      [
        'index' => 2,

        'extraClasses' => 'blogPage d-flex h-100 ',
        'title' => 'Enoteche'
      ]
    ]
  ],
  [
    'title' => 'Contessa di Radda',
    'subtitle' => 'Vigneto Toscano<br>Prestigioso dal 1500',
    'index' => 4,
    'background' => '4-contessa-di-radda.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '4-contessa-di-radda.jpg',
        'extraClasses' => 'd-flex h-100 backgroundOverlay justify-content-center',
        'title' => 'Contessa di Radda'
      ],
      [
        'index' => 1,
        'extraClasses' => 'stdPage  d-flex h-100 ',
        'title' => 'Storia',
        'has_common_gallery' => true
      ],
      [
        'index' => 2,
        'extraClasses' => 'galleryPage d-flex h-100 ',
        'title' => 'Galleria'
      ]
    ]
  ],
  [
    'title' => 'IFIS',
    'subtitle' => 'Progetto di filiera',
    'index' => 5,
    'background' => '5-progetto-di-filiera.jpg',
    'slides' => [
      [
        'index' => 0,
        'background' => '5-progetto-di-filiera.jpg',
        'extraClasses' => 'd-flex h-100 backgroundOverlay justify-content-center',
        'title' => 'Per I nostri fornitori'
      ],

      [
        'index' => 1,
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'Partnership'
      ],
      
      [
        'index' => 2,
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'Partnership'
      ],
      [
        'index' => 3,
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'Banca Ifis'
      ],
    /*  [
        'index' => 3,
        'extraClasses' => 'stdPage d-flex h-100 ',
        'title' => 'La Storia'
      ] */

    ]
  ]
];

$activSectionIndex = 0;
$imagesPath = 'assets/images/';
$siteUrl = 'https://chiantigeografico.it/';
#$siteUrl = 'http://localhost/geografico/';


?>
<!doctype html>
<html lang="en">
<head>

  <!--<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="3d141800-619a-4c5a-8adc-7c3446d8f627" data-blockingmode="auto" type="text/javascript"></script>-->
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K2KK22L');</script>
<!-- End Google Tag Manager -->
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=htmlentities($meta['description'])?>"
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/style.css?v=<?=rand(0,100000)?>">
  <link rel="stylesheet" href="assets/js/fullpage/src/fullpage.css">
  <script src="assets/js/fullpage/src/fullpage.js"></script>
  <script src="assets/js/fullpage/fullpage.resetSliders.min.js"></script>
  <script src="assets/js/fullpage/dist/fullpage.extensions.min.js"></script>
  <link rel="stylesheet" href="assets/js/slick/slick.css">
  <link rel="stylesheet" href="assets/js/slick/slick-theme.css">

  <link href="https://fonts.googleapis.com/css?family=Barlow:100,100i,300,400,400i,500,600,700|Cormorant+Garamond:300,400,500,600,700|Pinyon+Script&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <title>Geografico</title>
  <style media="screen">

  .slidingMenu .slidingMenuContent ul li strong{
    background-image: url('<?=$siteUrl.$imagesPath?>title-pin.png');
  }

  .stdPage h3{
    background-image: url('<?=$siteUrl.$imagesPath?>title-pin.png');
  }
  .fp-controlArrow.fp-prev {
    left: 60px;
    border: none;
    width: 50px;
    height: 101px;
    background-image: url('<?=$siteUrl.$imagesPath?>arrow-left.png') ;
    background-repeat: no-repeat;
    opacity: 0.5;
    cursor: pointer;
  }
  .fp-controlArrow.fp-next {
    right: 60px;
    border: none;
    width: 50px;
    height: 101px;
    background-image: url('<?=$siteUrl.$imagesPath?>arrow-right.png') ;
    background-repeat: no-repeat;
    opacity: 0.5;
    cursor: pointer;
  }
  .winesPageCarousel .descrLineTitle{

    background-image: url('<?=$siteUrl?>sections/3/slides/1/images/bullet.png');
    background-position: left center;
    background-repeat: no-repeat;

  }
  </style>
  <script type="text/javascript">

  var sections = <?=json_encode($sections)?>;
  var imagesPath = '<?=$imagesPath?>';
  </script>
  <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?theme=flying&always=1&showNoConsent=1&privacyPage=http%3A%2F%2Fchiantigeografico.it%2F"></script>

</head>
<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K2KK22L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <a href="javascript:" onclick="$('.slidingMenu').removeClass('out')" class="goHomeBtn"> <i class="fas fa-bars"></i> Home</a>
  <div class="leftBar">
    <!--<a target="_blank" class="youtube-link" href="<?=$social_links['youtube']?>" style="background-image:url('<?=$imagesPath?>icon-bg-youtube.png')">Youtube </a>-->
    <a target="_blank" class="instagram-link" href="<?=$social_links['instagram']?>" style="background-image:url('<?=$imagesPath?>icon-bg-instagram.png')">Instagram </a>
    <a target="_blank" class="facebook-link" href="<?=$social_links['facebook']?>" style="background-image:url('<?=$imagesPath?>icon-bg-facebook.png')">Facebook </a>
  </div>
  <div class="rightBar">
    <div class="sectionName">

    </div>
    <div class="sectionsList">
      <?php

      foreach($sections as $index => $section){

        ?>
        <div class="sectionItem" onclick="fullpage_api.moveTo(<?=($section['index']+1)?>, 0);">
          <div class="littleSquare">
          </div>
        </div>
        <?php

      }
      ?>
    </div>
    <div class="copyright">
      &reg; All rights reserved
    </div>
  </div>
  <div id="fullpage">

    <?php foreach($sections as $section){?>
      <div class="section <?=($section['index'] == $activSectionIndex) ? 'active' : ''?>" id="section<?=$section['index']?>" data-anchor="sectionAnchor<?=$section['index']?>">
        <?php if($section['index'] > 0){?>
          <div class="sectionIndex">
            0<?=$section['index']?>
          </div>
          <?php
        }
        if(isset($section['slides'])){
          if(count($section['slides'])>0){
            foreach($section['slides'] as $slide){?>
              <div class="fullpageslide">
                <div
                class="container-fluid pl-0 pr-0 vh-100 vw-100p "
                style="<?=isset($slide['background']) ? 'background-size:cover; background-image:url(\''.$imagesPath.$slide['background'].'\')' :  ''?>"
                >
                <div class="<?=(isset($slide['extraClasses'])) ? $slide['extraClasses'] : ''?>" data-ancor="slideAnchor<?=$section['index']?>-<?=$slide['index']?>">
                  <?php include 'sections/'.$section['index'].'/slides/'.$slide['index'].'/slide.php';?>
                </div>
              </div>
            </div>

            <?php
          }
        }
      }else{
        include 'sections/'.$section['index'].'/section.php';
      }


      ?>

    </div>
  <?php }?>
</div>

<?php include 'commons/slidingMenu.php'; ?>
<?php for($i = 0; $i<= 11; $i++){?>
  <img src="sections/3/slides/1/images/<?=$i?>.png" class="d-none">
<?php }?>


<!-- Modal -->
<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Privacy</h5>
      </div>
      <div class="modal-body">
        <?php include 'commons/privacy.php'?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>

<div id="topCenterFixedLogo">
  <img src="<?=$imagesPath?>geografico-logo.png">
</div>

<div id="bottomLeftLinks">
  <a href="#" data-toggle="modal" data-target="#privacyModal">Privacy</a>
</div>

<div id="bottomRightLinks">
  P.I. 00110500527
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" type="text/javascript"></script>
<script src="assets/js/scripts.js?v=<?=rand(0,1000000)?>" type="text/javascript"></script>
<!--<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="3d141800-619a-4c5a-8adc-7c3446d8f627" data-blockingmode="auto" type="text/javascript"></script>-->
</body>
</html>
