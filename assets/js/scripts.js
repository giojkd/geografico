
var is_resized = 0;

$(window).on("mousemove",function(e){
  if(is_resized == 0){
    positionWinePage();
    is_resized = 1;
  }
})

var myVideo = document.getElementById("geograficoVideo");

function updateSlideIndex(index){

  $('.sectionItem').removeClass('active');
  $('.sectionItem').eq(index).addClass('active');
  $('.sectionName').html(sections[index].title+' | ');

}

var hideArrows =  []; /* ['ssi_2_4','ssi_2_3','ssi_4_2'];*/
var hideArrowLeft = ['ssi_3_1'];
var darkLeftBar = ['ssi_0_1','ssi_1_0','ssi_4_1','ssi_3_1','ssi_3_2','ssi_1_1','ssi_1_2','ssi_1_3','ssi_1_4','ssi_2_1','ssi_2_2','ssi_5_1','ssi_5_2','ssi_5_3','ssi_1_5'];
var hideRightBar = ['ssi_4_2'];
function toggleArrows(section, slide){

  updateSlideIndex(section);

  $('.fp-controlArrow').show();

  var sectionSlideIndex = 'ssi_'+section+'_'+slide;

  if(slide > 0 ){
    fullpage_api.setAllowScrolling(false,'down');
    fullpage_api.setAllowScrolling(false,'up');
  }else{
    fullpage_api.setAllowScrolling(true);
  }

  console.log(sectionSlideIndex);

  if(sectionSlideIndex == 'ssi_1_0'){
    myVideo.play();
  }else{
    myVideo.pause();
  }

  if(typeof sections[section]['slides'] != 'undefined'){
    $('.slidesNavSlideName').html(sections[section]['slides'][slide]['title']+ '<span class="bottomSquaresSeparator">|</span>');
  }



  //alert(sectionSlideIndex);

  if(darkLeftBar.includes(sectionSlideIndex)){
    $('.leftBar, .rightBar, .goHomeBtn, .fp-slidesNav').addClass('dark');
    $('.fp-controlArrow.fp-prev').css({"background-image":"url('"+imagesPath+"/arrow-left-dark.png')"})
    $('.fp-controlArrow.fp-next').css({"background-image":"url('"+imagesPath+"/arrow-right-dark.png')"})
    $('.youtube-link').css({"background-image":"url('"+imagesPath+"/icon-bg-youtube.png')"})
    $('.facebook-link').css({"background-image":"url('"+imagesPath+"/icon-bg-facebook.png')"})
    $('.instagram-link').css({"background-image":"url('"+imagesPath+"/icon-bg-instagram.png')"})
  }   else {
    $('.leftBar, .rightBar, .goHomeBtn, .fp-slidesNav').removeClass('dark');
    $('.fp-controlArrow.fp-prev').css({"background-image":"url('"+imagesPath+"/arrow-left.png')"})
    $('.fp-controlArrow.fp-next').css({"background-image":"url('"+imagesPath+"/arrow-right.png')"})
    $('.youtube-link').css({"background-image":"url('"+imagesPath+"/icon-bg-youtube-white.png')"})
    $('.facebook-link').css({"background-image":"url('"+imagesPath+"/icon-bg-facebook-white.png')"})
    $('.instagram-link').css({"background-image":"url('"+imagesPath+"/icon-bg-instagram-white.png')"})

  }

  if(hideRightBar.includes(sectionSlideIndex)){
    $('.rightBar').hide();
  }else{
    $('.rightBar').show();
  }


  if(hideArrows.includes(sectionSlideIndex)){
    $('.fp-controlArrow').hide();
  }

  if(slide == 0){
    $('.fp-controlArrow.fp-prev').hide();
  }

  if(hideArrowLeft.includes(sectionSlideIndex)){
    $('.fp-controlArrow.fp-prev').hide();
  }

}

var myFullpage = new fullpage('#fullpage', {

  licenseKey:'6440E212-03364D86-ADFE97B3-361EE154',
  touchSensitivity: 15,


  slideSelector: '.fullpageslide',
  resetSliders:true,
  slidesNavigation:true,
  resetSlidersKey: 'Y2hpYW50aWdlb2dyYWZpY28uaXRfZnoxY21WelpYUlRiR2xrWlhKejRVVw==',
  afterSlideLoad:function(section, origin, destination, direction){

  },
  afterLoad: function(origin, destination, direction){
    positionWinePage();
    toggleArrows(destination.index, 0);
  },
  afterSlideLoad: function(section, origin, destination, direction){
    positionWinePage();
    toggleArrows(section.index, destination.index);
  },

  onLeave: function(origin, destination, direction){
    console.log(destination.index);
    section = fullpage_api.getActiveSection();
    //updateSlideIndex(destination.index)

  },
  onSlideLeave: function(section, origin, destination, direction){

    //updateSlideIndex(destination.index);

    /*
    console.log('section:');
    console.log(section);
    console.log('origin: ');
    console.log(origin);
    console.log('destination: ');
    console.log(destination);
    console.log('direction: '+direction);
    */
  }
});


function updateCustomIndicatorsCarousel(selector){

}


function valleyRollovers()
{
  $selector = $(".floatingValley");
  XAngle = 0;
  YAngle = 0;
  Z = 50;

  $selector.on("mousemove",function(e){
    var $this = $(this);
    var XRel = e.pageX - $this.offset().left;
    var YRel = e.pageY - $this.offset().top;
    console.log('x '+XRel+' '+'y '+YRel);
    var width = $this.width();

    YAngle = -(0.5 - (XRel / width)) * 40;
    XAngle = (0.5 - (YRel / width)) * 40;
    updateView($this.children(".icon"));
  });

  $selector.on("mouseleave",function(){
    oLayer = $(this).children(".icon");
    oLayer.css({"transform":"perspective(525px) translateZ(0) rotateX(0deg) rotateY(0deg)","transition":"all 150ms linear 0s","-webkit-transition":"all 150ms linear 0s"});
    oLayer.find("strong").css({"transform":"perspective(525px) translateZ(0) rotateX(0deg) rotateY(0deg)","transition":"all 150ms linear 0s","-webkit-transition":"all 150ms linear 0s"});
  });
}

function updateView(oLayer)
{
  oLayer.css({"transform":"perspective(525px) translateZ(" + Z + "px) rotateX(" + XAngle + "deg) rotateY(" + YAngle + "deg)","transition":"none","-webkit-transition":"none"});
  oLayer.find("strong").css({"transform":"perspective(525px) translateZ(" + Z + "px) rotateX(" + (XAngle / 0.66) + "deg) rotateY(" + (YAngle / 0.66) + "deg)","transition":"none","-webkit-transition":"none"});
}

function positionWinePage(){
  var winePhoto =   $('.winePhoto');
  var kakiBg = $('.kaki-bg');
  kakiBg.css({
    'height':($('.winePhoto').outerHeight()+30+30)+'px',
  })
  var floatingValley = $('.floatingValley');
  floatingValley.css({
    'margin-top': ((kakiBg.outerHeight()-floatingValley.outerHeight())/2)
  })


  var emptyBlackBox = $('.empty-black-box');
  emptyBlackBox.css({
    'height':kakiBg.outerHeight()+'px',
    'width':kakiBg.outerWidth()+'px',
    'margin-left':'-'+40+'px',
    'bottom':'-'+40+'px',
    'z-index':'-1'
  })

  return;

  kakiBg.closest('.col-md-6').css({
    'height':(height+40)+'px'
  })

  $('.carouselControls').css({
    'top':(height-180)+'px',
    'left':(width+75)+'px'
  });

}

positionWinePage();

var currentWineId = 0;

function changeWineAction(newWineId){

  var index = 0;
  for(var i in products){
    for(var j in products[i]['products']){

      if(index == newWineId){
        currentProduct = products[i]['products'][j];

        $('#downloadTechnicalSheet').attr('href','assets/schede_vini/'+currentProduct['ts']+'.pdf')

        for(var w in currentProduct){
          $('#'+w).html(currentProduct[w])
        }
        var cnt = 0;
        $('.descrLine').each(function(){
          var dl = $(this);
          cnt++;
          setTimeout(function(){
            dl.addClass('up');
            setTimeout(function(){
              dl.removeClass('up')
            }, 700);

          },cnt*150)
        })

        $('.nameTitle').addClass('up')
        setTimeout(function(){
          $('.nameTitle').html(currentProduct['name_modded']).removeClass('up');
          $('.nameTitleWrapper').css({'height':$('.nameTitle').outerHeight()+'px'})
        }, 1500);

        $('.appellationTitle').addClass('up')
        setTimeout(function(){
          $('.appellationTitle').html(currentProduct['appellation']).removeClass('up');
        }, 1500);




        console.log(currentProduct['subappellation']);

        for(var index in currentProduct){
          if(currentProduct[index] != ''){
            $( "#"+index+"Wrapper" ).animate({
              opacity: 1,
              height: "66px"
            }, 500, function() {
              // Animation complete.
            });
          }else{
            $(  "#"+index+"Wrapper" ).animate({
              opacity: 0,
              height: "0px"
            }, 500, function() {
              // Animation complete.
            });
          }
        }




        //$('.winePhoto').attr('src','sections/3/slides/1/images/'+newWineId+'.png');
        $('.winePhoto').fadeTo( "slow", 0 ,function(){
          $(this).attr('src','sections/3/slides/1/images/'+newWineId+'.png').fadeTo( "slow", 1 )
        });

        console.log(newWineId);
        $('.wineSelect').removeClass('active');
        $('#wineSelect-'+newWineId).addClass('active');
        var from_card_num = parseInt($('#wineSelect-'+currentWineId).closest('.card').data('product_card'));
        var to_card_num = parseInt($('#wineSelect-'+newWineId).closest('.card').data('product_card'));
        if(from_card_num != to_card_num){
          $('#wineSelect-'+newWineId).closest('.card').find('.collapse').collapse('show');
          $('.wineListCaret').removeClass('open')
          $('#wineSelect-'+newWineId).closest('.card').find('.wineListCaret').addClass('open');
        }

        currentWineId = newWineId;
        return;
      }else{
        index++;
      }

    }
  }

}

function changeWine(direction){
  var  newWineId = 0;
  if(currentWineId > 0 && direction == -1 || currentWineId < countProducts && direction == 1){
    newWineId = currentWineId + direction;
  }
  if(currentWineId<=0 && direction == -1){

    newWineId = (countProducts-1)
  }
  if(currentWineId >= (countProducts-1) && direction == 1){
    newWineId = 0;
  }
  changeWineAction(newWineId);
}

$(function(){

  $('.fullPageFloatingBtn').click(function(){
    var href = $(this).find('a').attr('href');
    location.replace(href);

  })

  const slickSlider = $('.slick-carousel')
  slickSlider
  .slick({
    arrows:false,
    dots: true,
    vertical: true,
    verticalSwiping: true,
    dotsClass:'d-none'
  });

  slickSlider.on('wheel', (function(e) {
    e.preventDefault();
    console.log(e.originalEvent.deltaY);
    if (e.originalEvent.deltaY < -90) {
      $(this).slick('slickNext');
    }
    if (e.originalEvent.deltaY > 90)
    {
      $(this).slick('slickPrev');
    }
  }));

  $('body').bind('mousewheel', function(e){
    var deltaX = e.originalEvent.deltaX;
    if(deltaX > 15){
      fullpage_api.moveSlideRight();

    }
    if(deltaX < -15){
      fullpage_api.moveSlideLeft();

    }
  });


  $('.fp-slidesNav.fp-bottom').prepend('<div class="slidesNavSlideName">SLIDE NAME</div>')

  $('.nameTitleWrapper').css({'height':$('.nameTitle').outerHeight()+'px'})

  $('.descrLine').wrap('<div class="descrLineWrapper"></div>');
  $(window).resize(function(){
    is_resized = 1;
    positionWinePage();
  })
  $('.homepageColumns .col-content').hover(function(){
    var bg = $(this).data('background');
    $('.homepageColumns').css({
      'background-image':"url('assets/images/"+bg+"')"
    })
  })
  valleyRollovers();
  positionWinePage();
  $('#productsSlider').on('slide.bs.carousel', function (e) {
    var toSlide = e.to;
    var fromSlide = e.from ;
    $('.wineSelect').removeClass('active');
    $('#wineSelect-'+toSlide).addClass('active');
    var from_card_num = parseInt($('#wineSelect-'+fromSlide).closest('.card').data('product_card'));
    var to_card_num = parseInt($('#wineSelect-'+toSlide).closest('.card').data('product_card'));
    if(from_card_num != to_card_num){
      $('#wineSelect-'+toSlide).closest('.card').find('.collapse').collapse('show');
    }



  })
})
