<div class="slidingMenu out" onclick="$(this).addClass('out')">
  <div class="row">
    <div class="col-md-1 bg-white pr-0">

    </div>
    <div class="col-md-3 pl-0">
      <div class="d-flex vh-100 bg-white">
        <div class="slidingMenuContent align-self-center">
          <ul class="list list-unstyled">
            <?php
            $sectionsMenu = array_merge($sections,[
                [
                    'title' => 'Shop',
                    'subtitle' => 'Acquista i nostri vini',
                    'index' => 3,
                    'slide' => 2,
                    'background' => 'PicciniVigneto.png',
                    'href' => $spsl_default['wine_shop'],
                ],[
                    'title' => 'Contatti',
                    'subtitle' => 'Vieni a trovarci',
                    'index' => 3,
                    'slide' => 2,
                    'background' => 'PicciniVigneto.png',
                ]
            ]);
            foreach($sectionsMenu as $section){
                if(isset($section['href'])){?>
                    <li onclick="window.location.replace('<?=$section['href']?>')">
                        <strong><?=$section['title']?></strong>
                        <span><?=str_replace('<br>',' ',$section['subtitle'])?></span>
                    </li>
               <?php }else{
              ?>
              <li onclick="fullpage_api.moveTo(<?=($section['index']+1)?>,<?=(isset($section['slide']) ? $section['slide'] : 0)?>)">
                <strong><?=$section['title']?></strong>
                <span><?=str_replace('<br>',' ',$section['subtitle'])?></span>
              </li>
            <?php }}
            ?>
          </ul>



        </div>
      </div>
        <div class="close"></div>
    </div>
  </div>
</div>
