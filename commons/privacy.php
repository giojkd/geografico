<p><strong>
  NB
  Questo sito non utilizza cookie di profilazione propri e non utilizza cookie di profilazione di terze parti. <br>
  Se utilizzato, Google Analytics è stato anonimizzato, il che permette di raccogliere statistiche senza profilare gli utilizzatori. </strong><br>
  <br>
  INFORMATIVA UTENTI INTERNET IN MATERIA DI PROTEZIONE DEI DATI PERSONALI<br>
  AI SENSI DELL’ART. 13 D. LGS. 30.06.2003, N. 196<br>
  In osservanza di quanto previsto ai sensi e per gli effetti dell’art. 13 D. Lgs 30 giugno 2003, Chianti Geografico - Via Mulinaccio, 10 - 53013 Gaiole in Chianti (SI) - Italia - P.I. 00110500527 - info@chiantigeografico.it, nella sua qualità di titolare, informa l’utente su quali siano le finalità e modalità del trattamento dei dati personali raccolti, il loro ambito di comunicazione e diffusione, oltre alla natura del loro conferimento.<br><br>
</p>
<p>FINALITÀ DEL TRATTAMENTO:<br>
  I dati raccolti presso gli interessati costituiscono dati oggetto del trattamento e sono trattati ed utilizzati direttamente per adempiere a finalità strumentali alla richiesta inviata dagli interessati stessi;<br>
  Sono, altresì, trattati ed utilizzati direttamente per consentire l’invio di informazioni di natura commerciale, la promozione delle nostre attività e l’erogazione dei nostri servizi, attraverso tutti gli strumenti utilizzati dall’azienda, quali newsletter, posta cartacea, SMS, MMS, etc.<br><br>
</p>
<p>MODALITÀ DI TRATTAMENTO:<br>
  Il trattamento dei dati è eseguito attraverso procedure informatiche o comunque mezzi telematici o supporti cartacei ad opera di soggetti interni appositamente incaricati. I dati sono conservati in archivi cartacei, informatici e telematici e sono assicurate le misure di sicurezza minime previste dal legislatore.<br><br>
</p>
<p>COMUNICAZIONE E DIFFUSIONE:<br>
  I dati personali non saranno diffusi, venduti o scambiati con soggetti terzi, salvo qualora la comunicazione si renda necessaria per il perseguimento delle finalità suddette.<br><br>
</p>
<p>DIRITTI DELL’INTERESSATO:<br>
  L’interessato potrà far valere i propri diritti come espressi dall’artt. 7, 8, 9 e 10 del D.Lgs. 30 giugno 2003 n. 196, rivolgendosi al titolare del trattamento. In particolare secondo l’art. 7 l’interessato potrà ottenere la conferma dell’esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati e la loro comunicazione in forma intelligibile.<br><br>
</p>
<p>L’interessato ha diritto di ottenere l’indicazione:<br>
  a) dell’origine dei dati personali;<br>
  b) delle finalità e modalità del trattamento;<br>
  c) della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti elettronici;<br>
  d) degli estremi identificativi del titolare, dei responsabili e del rappresentante designato ai sensi dell’articolo 5, comma 2;<br>
  e) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati.<br><br>
</p>
<p>L’interessato ha diritto di ottenere:<br>
  a) l’aggiornamento, la rettificazione ovvero, quando vi ha interesse, l’integrazione dei dati;<br>
  b) la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;<br>
  c) l’attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato.<br><br>
</p>
<p>L’interessato ha diritto di opporsi, in tutto o in parte:<br>
  a) per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti allo scopo della raccolta;<br>
  b) al trattamento di dati personali che lo riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.<br><br>
</p>
<p>TEMPO DI CONSERVAZIONE:<br>
  I dati saranno conservati per il tempo strettamente necessario all’espletamento delle suddette finalità, salvo il diritto di opposizione al trattamento liberamente esercitabile dall’utente.<br><br>
</p>
<p>COOKIES:<br>
  Questo sito può fare uso di cookies tecnici che permettono al sito di funzionare correttamente. Veicola inoltre cookies di terze parti come da informativa estesa che puoi leggere di seguito.<br><br>
</p>
<p>NATURA DEL CONFERIMENTO, CONSEGUENZE RIFIUTO A RISPONDERE E CONSENSO:<br>
  Il conferimento dei dati è facoltativo ed è rimesso alla volontà del soggetto che voglia richiedere informazioni ed intraprendere trattative con il Titolare. Il mancato conferimento comporta l’impossibilità di dar seguito alle richieste. Ai sensi dell’art. 23 del D. Lgs. 196/2003, il consenso al trattamento dei suddetti dati è necessario in quanto gli stessi sono raccolti fuori da accordi contrattuali pregressi. Il consenso è altresì necessario per l’eventuale attività promozionale e di comunicazione di natura commerciale della società (attraverso strumenti quali newsletter, posta cartacea, SMS, MMS, etc.).<br><br>
</p>
<p>TITOLARE DEL TRATTAMENTO:<br>
  Il Titolare è Chianti Geografico - Via Mulinaccio, 10 - 53013 Gaiole in Chianti (SI) - Italia - P.I. 00110500527 - info@chiantigeografico.it</p>
  <p>&nbsp;</p><br><br>
  <p><strong>INFORMATIVA SUI COOKIE</strong><br><br>
  </p>
  Questo sito può utilizzare cookies, anche di terze parti, per migliorare l’esperienza di navigazione e consentire a chi naviga di usufruire dei nostri servizi online.
  La presente policy sui cookies deve essere letta insieme alla nostra Privacy Policy.
  Se prosegui nella navigazione del nostro sito acconsenti all’utilizzo dei cookies.<br><br>
  <p>COSA SONO I COOKIES<br><br>
    I cookies sono piccoli file di testo inviati da siti web e memorizzati sul vostro computer, tablet, smartphone o altro dispositivo mobile. Essi contengono informazioni di base sulla navigazione in Internet e grazie al browser vengono riconosciuti ogni volta che l’utente visita il sito. I cookies possono anche essere cookies di prime parti se impostati dal sito visitato, oppure cookies di terze parti se inviati da un sito diverso da quello visitato. Si distinguono due macro-categorie di cookies, quelli “tecnici” e quelli “di profilazione”.<br><br>
  </p>
  <p>CATEGORIE DEI COOKIES<br><br>
    <strong>NB
      Questo sito non utilizza cookie di profilazione propri e non utilizza cookie di profilazione di terze parti. <br>
      Se utilizzato, Google Analytics è stato anonimizzato, il che permette di raccogliere statistiche senza profilare gli utilizzatori.</strong><br>
      <br>
    </p>
    <p>a) Cookies tecnici<br>
      Questa tipologia di cookies permette il corretto funzionamento di alcune sezioni del sito. Sono di due categorie, persistenti e di sessione:<br>
    </p>
    <p>- persistenti: una volta chiuso il browser non vengono distrutti ma rimangono fino ad una data di scadenza preimpostata o finché l’utente non li elimina<br>
      - di sessione: vengono distrutti ogni volta che il browser viene chiuso<br>
      <br>
    </p>
    <p>Questi cookies sono necessari a visualizzare correttamente il sito e in relazione ai servizi tecnici offerti, verranno quindi sempre utilizzati e inviati, a meno che l’utente non modifichi le impostazioni nel proprio browser.<br>
      <br>
    </p>
    <p>I cookies tecnici vengo utilizzati per le seguenti attività:<br>
      - Attività strettamente necessarie al funzionamento<br>
      - Questi cookies hanno natura tecnica e permettono al sito di funzionare correttamente. Ad esempio, mantengono l’utente collegato durante la navigazione evitando che il sito richieda di collegarsi più volte per accedere alle varie pagine o sezioni del sito.<br>
      - Attività di salvataggio delle preferenze<br>
      - Questi cookies permettono di ricordare le preferenze selezionate dall’utente durante la navigazione, ad esempio, consentono di impostare la lingua.<br>
      - Attività Statistiche e di Misurazione dell’audience<br>
      - Questi cookies ci aiutano a capire, attraverso dati raccolti in forma anonima e aggregata, come gli utenti interagiscono con il nostro sito internet fornendoci informazioni relative alle sezioni visitate, il tempo trascorso sul sito, eventuali malfunzionamenti. Questo ci aiuta a migliorare la resa del nostro sito internet.<br>
      <br>
    </p>
    <p>b) Cookies di profilazione<br>
      I cookies di profilazione sono volti a creare profili relativi all’utente e vengono utilizzati al fine di inviare messaggi pubblicitari in linea con le preferenze manifestate dall’utente nell’ambito della navigazione in rete.<br>
      <br>
    </p>
    <p>COOKIE DI TERZA PARTE<br>
      Possono a loro volta installare cookies per il corretto funzionamento dei servizi che stanno fornendo, sui quali non abbiamo alcun controllo. Se desiderate avere informazioni relative a questi cookies di terza parte e su come disabilitarli vi preghiamo di accedere ai link di collegamento sotto riportati.<br>
      <br>
    </p>
    <p>ISTRUZIONI DISABILITAZIONE COOKIES DAI BROWSERS<br>
      <br>
      disabilitazione dei cookie su <a href="https://support.mozilla.org/it/kb/Attivare%20e%20disattivare%20i%20cookie" target="_blank" style="color: #01749f;">Firefox</a>
      <br>
      disabilitazione dei cookie su <a href="http://www.google.com/policies/technologies/managing/" target="_blank" style="color: #01749f;">Chrome</a>
      <br>
      disabilitazione dei cookie su <a href="http://windows.microsoft.com/it-it/internet-explorer/delete-manage-cookies#ie=ie-11" target="_blank" style="color: #01749f;">Internet Explorer</a>
      <br>
      disabilitazione dei cookie su <a href="http://support.apple.com/kb/HT1677?viewlocale=it_IT&amp;locale=it_IT" target="_blank" style="color: #01749f;">Safari</a>
      <br>
      disabilitazione dei cookie su <a href="http://help.opera.com/Mac/12.10/it/cookies.html" target="_blank" style="color: #01749f;">Opera</a>

      <br>
      <br>
      Se usi un qualsiasi altro browser, cerca nelle impostazioni del browser la modalità di gestione dei cookies.
      Per saperne di più riguardo la gestione dei cookies sul browser dei dispositivi mobili, consultare il manuale utente specifico. Puoi anche trovare ulteriori informazioni sui cookies e su come gestirli consultando il sito <a href="http://www.aboutcookies.org/" target="_blank" style="color: #01749f;">www.aboutcookies.org</a>.
      Inoltre accedendo alla pagina <a href="http://www.youronlinechoices.com/it/le-tue-scelte" target="_blank" style="color: #01749f;">http://www.youronlinechoices.com/it/le-tue-scelte</a> è possibile informarsi sulla pubblicità comportamentale oltre che disattivare o attivare le società elencate e che lavorano con i gestori dei siti web per raccogliere e utilizzare informazioni utili alla fruizione della pubblicità.
      <br>
    </p>
    <p class="narrow">
    </p>
