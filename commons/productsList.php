<?php


$products = [
  [
    'category_name' => 'Borgo alla terra',
    'products' => [
      [
        'name' => 'Chianti Colli Senesi',
        'name_modded' => 'Borgo alla<br>terra',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine',
        'appellation' => 'Chianti DOCG',
        'subappellation' => 'Colli Senesi',
        'blend' => 'Sangiovese 90% - Canaiolo 10%',
        'blend' => 'Sangiovese e uvaggi bacca rossa coltivate sulle colline senesi',
        'region' => 'Toscana | Tuscany',
        'soil' => 'Argilloso sabbioso | Sandy Clayey',
        'ts' => 'borgo_alla_terra_chianti_colli_senesi'
      ],
      [
        'name' => 'Vernaccia di San Gimignano',
        'name_modded' => 'Borgo alla<br>terra',
        'wine_type' => 'Vino bianco fermo secco | Dry white still wine ',
        'appellation' => 'Vernaccia di San Gimignano DOCG',
        'subappellation' => 'San Gimignano',
        'blend' => 'Vernaccia di San Gimignano 100% ',
        'blend' => 'Vernaccia e uve bianche prodotte nel comune di San Gimignano.',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso con ciottoli | Clayey - sandy stratum and pebbles',
        'ts' => 'borgo_alla_terra_vernaccia_di_san_gimignano'
      ],
        [
            'name' => 'Chianti',
            'name_modded' => 'Borgo alla<br>terra',
            'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
            'appellation' => 'Chianti DOCG',
            'subappellation' => '',
            'blend' => 'Vernaccia di San Gimignano 100% ',
            'blend' => 'Sangiovese e varietà bacca rossa prodotte in Toscana | Sangiove and red grapes varietals grown in Tuscany',
            'region' => 'Toscana | Tuscany ',
            'soil' => 'Argilloso sabbioso con ciottoli | Sandy clayey pebbles',
            'ts' => 'borgo_alla_terra_chianti'
        ],
        [
            'name' => 'Chianti Riserva DOCG',
            'name_modded' => 'Borgo alla<br>terra',
            'wine_type' => 'VVino rosso fermo secco | Dry red still wine',
            'appellation' => 'Chianti Riserva DOCG',
            'subappellation' => '',
            'blend' => 'Vernaccia di San Gimignano 100% ',
            'blend' => 'Sangiovese e varietà bacca rossa prodotte in Toscana | Sangiove and red grapes varietals grown in Tuscany',
            'region' => 'Toscana | Tuscany ',
            'soil' => 'Argilloso sabbioso con ciottoli | Clayey and fragmented rock',
            'ts' => 'borgo_alla_terra_chianti_riserva'
        ]
    ]
  ],
  [
    'category_name' => 'Bosco del Grillo',
    'products' => [
      [
        'name' => 'Governo all\'Uso Toscano IGT',
        'name_modded' => 'Bosco del<br>Grillo',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
        'appellation' => 'Governo all\'Uso Toscano IGT',
        'subappellation' => '',
        'blend' => 'Sangiovese 80%, Merlot 10%, Colorino 5%, Cabernet 5% ',
        'blend' => 'Blend a base di Sangiovese',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso con ciottoli | Clay and rock ',
        'ts' => 'bosco_del_grillo_governo_all_uso_toscano'
      ]
    ]
  ],
  /*[
    'category_name' => 'Capofosso',
    'products' => [
      [
        'name' => 'Chianti DOCG',
        'name_modded' => 'Capofosso',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
        'appellation' => 'Chianti DOCG',
        'subappellation' => '',
        'blend' => 'Sangiovese 90% - Canaiolo 10% ',
        'blend' => 'Sangiovese e varietà a bacca rossa prodotte in Toscana',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso con ciottoli | Sandy clayey pebbles ',
        'ts' => 'capofosso_chianti_docg'
      ],
      [
        'name' => 'Chianti Riserva DOCG ',
        'name_modded' => 'Capofosso',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine',
        'appellation' => 'Chianti Riserva DOCG ',
        'subappellation' => '',
        'blend' => 'Sangiovese 95%, Cabernet 5% ',
        'blend' => 'Sangiovese e varietà a bacca rossa prodotte in Toscana',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso con ciottoli | Clay and
        fragmented rock ',
        'ts' => 'bosco_del_grillo_chianti_riserva_docg'
      ]

    ]
  ],*/
  [
    'category_name' => 'Cerraia',
    'products' => [
      [
        'name' => 'Vino Nobile di Montepulciano ',
        'name_modded' => 'Cerraia',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine  ',
        'appellation' => 'Nobile di Montepulciano DOCG ',
        'subappellation' => 'Montepulciano',
        'blend' => 'Prugnolo Gentile 85% - Colorino, Canaiolo Nero, Merlot 15%  ',
        'blend' => 'Prugnolo Gentile e uve a bacca rossa prodotte nella zona di Montepulciano',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso | Clayey with sandy stratum  ',
        'ts' => 'cerraia_vino_nobile_di_montepulciano'
      ]
    ]
  ],
/*  [
    'category_name' => 'Ferraiolo',
    'products' => [
      [
        'name' => 'Toscana IGT ',
        'name_modded' => 'Ferraiolo',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
        'appellation' => 'Toscana IGT ',
        'subappellation' => '',
        'blend' => 'Sangiovese 70% - Cabernet Sauvignon 30% ',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso | Sandy clayey '
      ]
    ]
  ],*/
  [
    'category_name' => 'Contessa di Radda',
    'products' => [
      [
        'name' => 'Chianti Classico',
        'name_modded' => 'Contessa di<br>Radda',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine',
        'appellation' => 'Chianti classico DOCG',
        'subappellation' => 'Radda in Chianti - Castellina in Chianti ',
        'blend' => 'Sangiovese 95% - Merlot 5%',
        'blend' => 'Sangiovese e Merlot',
        'region' => 'Toscana | Tuscany',
        'soil' => 'Eocene, calcareo misto e marna | Eocene, mixed
        calcareous and marl ',
        'ts' => 'contessa_di_radda_chianti_classico'
      ],
      [
        'name' => 'Chianti Classico Riserva',
        'name_modded' => 'Contessa di<br>Radda',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine  ',
        'appellation' => 'Chianti Classico Riserva DOCG ',
        'subappellation' => 'Radda in Chianti - Castellina in Chianti ',
        'blend' => 'Sangiovese90% -Cab.Sauvignon5%-Colorino5%  ',
        'blend' => 'Sangiovese, Cabernet. Sauvignon e Colorino',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Eocene, calcareo misto e marna | Eocene, mixed
        calcareous and marl ',
        'ts' => 'contessa_di_radda_chianti_classico_riserva'
      ],
        [
            'name' => 'IGT Toscana',
            'name_modded' => 'Contessa di<br>Radda',
            'wine_type' => 'Vino rosso fermo secco | Dry red still wine  ',
            'appellation' => 'Toscana IGT',
            'subappellation' => 'Radda in Chianti - Castellina in Chianti',
            'blend' => 'Blend a base di Merlot | Merlot based blend',
            #'blend' => 'Sangiovese, Cabernet. Sauvignon e Colorino',
            'region' => 'Toscana | Tuscany ',
            'soil' => 'Eocene, calcareo misto e marna | Eocene, mixed
        calcareous and marl ',
            'ts' => 'contessa_di_radda_igt'
        ]
    ]
  ],
  [
    'category_name' => 'Montegiachi',
    'products' => [
      [
        'name' => 'Chianti Classico Riserva DOCG',
        'name_modded' => 'Montegiachi',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
        'appellation' => 'Chianti Classico DOCG ',
        'subappellation' => '',
        'blend' => 'Sangiovese 90% - Merlot 10%  ',
        'blend' => 'Sangiovese unito ad una piccola percentuale di uve a bacca rossa',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Argilloso sabbioso | Clayey with sandy strata ',
        'ts' => 'montegiachi_chianti_classico_riservra_docg'
      ]
    ]
  ],
  /*[
    'category_name' => 'Pulleraia',
    'products' => [
      [
        'name' => 'IGT Toscana',
        'name_modded' => 'Pulleria',
        'wine_type' => 'Vino rosso fermo secco | Dry red still wine ',
        'appellation' => 'Toscana IGT  ',
        'subappellation' => '',
        'blend' => 'Merlot 100%  ',
        'region' => 'Toscana | Tuscany ',
        'soil' => 'Alluvionale calcareo | Alluvial soil '
      ]
    ]
  ],*/
  [
    'category_name' => 'Vinsanto',
    'products' => [
      [
        'name' => 'Vin Santo del Chianti',
        'name_modded' => 'Vinsanto',
        'wine_type' => 'Vino bianco fermo dolce | Sweet white wine ',
        'appellation' => 'Vin Santo del Chianti D.O.C.  ',
        'subappellation' => 'PRIMA RACCOLTA | FIRST VINTAGE 1997',
        'blend' => 'Trebbiano 80% - Malvasia 20% ',
        'blend' => 'Prevalentemente uve bianche provenienti dalla zona del Chianti',
        'region' => 'VENDEMMIA | HARVEST
        Settembre poi essiccamento a 10°-15°C fino a Dicembre  ',
        'soil' => ' Argilloso sabbioso con ciottoli | Clayey - sandy stratum
        and pebbles  ',
        'ts' => 'vinsanto_vin_santo_del_chianti'
      ]
    ]
  ]
];


?>
