
<?php
$showArrowsAndLogo = (isset($showArrowsAndLogo)) ? $showArrowsAndLogo : true;

if(isset($slide['has_common_gallery'])){
  $slideGalleryImagePrefix = ($slide['has_common_gallery'] == true) ? $siteUrl.'assets/images/' : $siteUrl.'sections/'.$section['index'].'/slides/'.$slide['index'].'/images/';
}else{
  $slideGalleryImagePrefix = $siteUrl.'sections/'.$section['index'].'/slides/'.$slide['index'].'/images/';
}
?>
<div class="row justify-content-center align-self-center">
  <div class="col-md-3">
    <div class="cavaliere" style="height: 30px"></div>
    <div id="slider-<?=$section['index']?>-<?=$slide['index']?>" class="carousel slide stdPageCarousel" data-ride="carousel">
      <div class="carousel-inner">
        <?php foreach($page['photos'] as $photoIndex => $photo){?>
          <div class="carousel-item <?=($photoIndex == 0) ? 'active' : ''?>">
            <img src="<?=$slideGalleryImagePrefix.$photo?>" class="d-block w-100" alt="...">
          </div>
        <?php }?>
      </div>
      <?php if($showArrowsAndLogo){?>

        <a class="carousel-control-prev carousel-control" href="#slider-<?=$section['index']?>-<?=$slide['index']?>" role="button" data-slide="prev">
          <i class="fa fa-angle-left"></i>
        </a>
        <a class="carousel-control-next carousel-control" href="#slider-<?=$section['index']?>-<?=$slide['index']?>" role="button" data-slide="next">
          <i class="fa fa-angle-right"></i>
        </a>
      <?php }?>
      <div class="emptySquaredBox">
        <div class="pageSuperTitle">
          <?=$page['superTitle']?>
        </div>
      </div>
    </div>
    <?php if($showArrowsAndLogo){?>
      <img class="geograficoLogo" src="assets/images/geografico-logo.png" alt="">
    <?php }?>
  </div>
  <div class="col-md-<?=(isset($page['col-width'])) ? $page['col-width'] : 4 ?> pl-5">
    <span class="superTitle"><?=$page['superTitle']?></span>
    <h1><?=$page['title']?></h1>
    <h2><?=$page['subtitle']?></h2>
    <div class="content">
      <?php foreach($page['description'] as $descr){?>
        <?=($descr['title'] != '') ? '<h3>'.$descr['title'].'</h3>' : '' ?>
        <p><?=$descr['content']?></p>
      <?php } ?>
    </div>
    <hr>
    <?php

    $hideStdPageSubIcons = (isset($hideStdPageSubIcons)) ? $hideStdPageSubIcons : false;
    if(!$hideStdPageSubIcons){
      include 'commons/stdPageSubIcons.php';
    }
    ?>
  </div>
</div>
