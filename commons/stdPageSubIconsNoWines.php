<?php


if(isset($spsl)){
  foreach($spsl as $index => $value){
    if(!isset($index)){
      $spsl[$index] = $spsl_default[$index];
    }else{
      if(trim($value) == ''){
        $spsl[$index] = $spsl_default[$index];
      }
    }
  }
}else{
  $spsl = $spsl_default;
}
?>

<div class="pageIconsListWrapper" >
  <!--<div class="d-inline-block align-top">
  <i class="fas fa-angle-down"></i>
</div>-->
<div class="stdPageIconsList">


  <div class="row">
    <div class="col mr-2">
      <a  href="<?=$spsl['gallery']?>"> <img src="<?=$imagesPath?>icons8-gallery.png" alt=""> Gallery</a>
    </div>
    <div class="col mr-2">

      <a href="<?=$spsl['wine_shop']?>">
        <img src="<?=$imagesPath?>icons8-add_shopping_cart.png" alt="">  Shop Vini
      </a>
    </div>
    <div class="col">
      <a href="<?=$spsl['reserve_tasting']?>">

        <img src="<?=$imagesPath?>icons8-bowl_with_spoon.png" alt=""> Prenota Degustazione
      </a>
    </div>
  </div>
</div>

</div>
