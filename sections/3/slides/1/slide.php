<?php

include 'commons/productsList.php';
?>
<script type="text/javascript">
var products = <?=json_encode($products)?>;
var currentProduct  = <?=json_encode($products[0]['products'][0])?>
</script>
<div class="row vw-100 ml-0">

  <div class="col-md-2 bg-mid-grey pl-0">
    <div class="d-flex vh-100 pl-5">
      <div class="justify-content-center align-self-center">
        <div class="accordion" id="winesListAccordion">
          <?php
          $productCount = 0;
          foreach($products as $index => $product){ ?>
            <div class="card" data-product_card="<?=$index?>" >
              <div class="card-header" id="wines-list-heading-<?=$index?>">
                <h2 class="mb-0">
                  <button class="btn text-nowrap btn-link <?=($index == 0) ? '' : 'collapsed'?>" type="button" data-toggle="collapse" data-target="#wines-list-<?=$index?>" aria-expanded="<?=($index == 0) ? 'true' : 'false'?>" aria-controls="wines-list-<?=$index?>">
                    <span class="wineListCaret <?=($index == 0) ? 'open' : ''?>"><i class="fas fa-angle-right"></i></span>  <?=$product['category_name']?> (<?=count($product['products'])?>)
                  </button>
                </h2>
              </div>
              <div id="wines-list-<?=$index?>" class="collapse <?=($index == 0) ? 'show' : ''?>" aria-labelledby="wines-list-heading-<?=$index?>" data-parent="#winesListAccordion">
                <div class="card-body">
                  <ul class="list list-unstyled">
                    <?php foreach($product['products'] as $item){?>
                      <li class="wineSelect text-nowrap <?=($productCount == 0) ? 'active' : ''?>" id="wineSelect-<?=$productCount?>" onclick="changeWineAction(<?=$productCount?>); $('.wineSelect').removeClass('active'); $(this).addClass('active')"><?=$item['name']?></li>
                      <?php
                      $productCount++;
                    } ?>
                  </ul>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  var countProducts = <?=$productCount?>;
  </script>
  <div class="col-md-9">
    <div class="d-flex vh-100">
      <div class="justify-content-center align-self-center vw-100p">
        <div id="productsSlider" class="carousel slide winesPageCarousel" data-interval="false" data-ride="carousel">
          <div class="">
            <?php
            $index = 0;
            $product = $products[0]['products'][0];

            $product = array_filter($product);
            ?>
            <div class="carousel-item <?=($index == 0) ? 'active' : ''?>">
              <div class="row">
                <div class="col-md-6 d-flex">
                  <div class="bottleWrapper justify-content-center align-self-center">
                    <div class="kaki-bg ">
                      <a href="assets/schede_vini/<?=$product['ts']?>.pdf" download id="downloadTechnicalSheet">
                        <img src="<?=$imagesPath?>icons8-pdf.png" height="20" alt=""> | Scarica la scheda prodotto</a>
                      </a>
                      <div class="floatingValley">
                        <div class="icon">
                          <img src="<?=$imagesPath?>sfondo_vini_3d.png" alt="">
                        </div>
                      </div>
                      <img class="winePhoto" src="sections/<?=$section['index']?>/slides/<?=$slide['index']?>/images/<?=$index?>.png" alt="">
                      <div class="carouselControls">
                        <a onclick="changeWine(-1)" class="carousel-control-prev carousel-control" href="#productsSlider" role="button" data-slide="prev">
                          <i class="fa fa-angle-left"></i>
                        </a>
                        <a onclick="changeWine(1)" class="carousel-control-next carousel-control" href="#productsSlider" role="button" data-slide="next">
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                      <div class="empty-black-box">
                        <div class="slideSideLabel">I vini</div>
                        <!--img class="geograficoLogo" src="<?=$imagesPath?>geografico-logo.png">-->
                      </div>

                    </div>
                    <br>


                  </div>
                </div>
                <div class="col-md-6" style="padding-left: 35px;">
                  <div class="wineCardHeader" style="background-image:url('<?=$imagesPath?>geografico-navigator.png'); background-size: contain; background-repeat: no-repeat;">


                    <span class="wineCardSuperTitle">I Nostri Vini</span>
                    <div class="nameTitleWrapper">
                      <div class="nameTitle"><?=$product['name_modded']?></div>
                    </div>
                    <div class="appellationTitleWrapper">
                      <div class="appellationTitle"><?=$product['appellation']?></div>
                    </div>
                  </div>
                  <div class="wineCardDescription">

                    <p class="descrLineTitle">Tipologia Vino | <span>Wine Typology</span> </p>
                    <p class="descrLine" id="wine_type"><?=$product['wine_type']?></p>
                    <p class="descrLineTitle">Denominazione | <span>Appellation</span> </p>
                    <p class="descrLine"  id="appellation"><?=$product['appellation']?></p>
                    <div id="subappellationWrapper">
                      <p class="descrLineTitle">Sottozona | <span>Subappellation</span> </p>
                      <p class="descrLine" id="subappellation"><?=$product['subappellation']?></p>
                    </div>
                    <p class="descrLineTitle">Uve | <span>Blend</span> </p>
                    <p class="descrLine" id="blend"><?=$product['blend']?></p>
                    <div id="regionWrapper">
                      <p class="descrLineTitle">Regione | <span>Region</span> </p>
                      <p class="descrLine" id="region"><?=$product['region']?></p>
                    </div>
                    <div id="firstvintageWrapper">
                      <p class="descrLineTitle">Prima raccolta | <span>First vintage</span> </p>
                      <p class="descrLine" id="firstvintage"></p>
                    </div>
                    <div id="harvestWrapper">
                      <p class="descrLineTitle">Vendemmia | <span>Harvest</span> </p>
                      <p class="descrLine" id="harvest"></p>
                    </div>
                    <p class="descrLineTitle">Suolo | <span>Soil</span> </p>
                    <p class="descrLine" id="soil"><?=$product['soil']?></p>
                  </div>
                  <div style="padding-left: 30px;">
                    <?php
                    include 'commons/stdPageSubIconsNoWines.php'; ?>
                  </div>
                </div>
              </div>
            </div>
            <?php
            $index++;

            ?>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
