<div class="row align-self-center fullPage">
  <div class="col-md-12  text-center">
    <div class="row justify-content-center align-self-center mb-4">
      <div class="col-md-6">
        <div class="fullPageFloatingBtnTop">
          <a href="#sectionAnchor2/3">
            <img src="<?=$imagesPath?>icons8-gallery.png" alt=""> | Gallery
          </a>
        </div>
      </div>
      <div class="col-md-6">
        <div class="fullPageFloatingBtnTop">
          <a href="<?=$spsl_default['wine_shop']?>">
            <img src="<?=$imagesPath?>icons8-add_shopping_cart.png" alt=""> | Shop
          </a>
        </div>
      </div>
    </div>
    <h1>Contessa di Radda</h1>
    <p class="fullPageIntro">

    </p>
    <div class="fullPageMiddleBox">
      <!--<img src="<?=$imagesPath?>arrowdown.png" alt="">-->
      <?php include 'commons/arrowScrollDown.php' ?>
    </div>
    <div class="row justify-content-center align-self-center">
      <div class="col-md-4 text-center">
        <div class="fullPageFloatingBtn">
          <a href="#sectionAnchor4/1">
            <img src="<?=$imagesPath?>icons8-activity_history.png" alt=""> La Storia
          </a>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="fullPageFloatingBtn">
          <a href="<?=$spsl_default['wine_cards']?>">
            <img src="<?=$imagesPath?>icons8-grapes-small.png" alt=""> I nostri vini
          </a>
        </div>
      </div>
      <div class="col-md-4 text-center">
        <div class="fullPageFloatingBtn">
          <a href="<?=$spsl_default['reserve_tasting']?>">
            <img src="<?=$imagesPath?>icons8-bowl_with_spoon.png" alt=""> Enoteche
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
