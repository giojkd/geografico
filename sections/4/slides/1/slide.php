<?php
$images = 23;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical_contessa_di_radda/Img_'.$i.'.jpg';
}
shuffle($gallery);
$page = [
  'superTitle' => 'Storia',
  'title' => 'Contessa di Radda',
  'subtitle' => 'La storia del Chianti Classico Contessa di Radda è intrisa di tradizione e cultura, oltre ad essere fortemente legata al suo territorio di appartenenza.',
  'description' => [
    [
      'title' => '',
      'content' => 'Il Chianti Classico Contessa di Radda, nasce nel 1978 e prende il nome dalla contessa Willa di Toscana, madre del celebre Marchese Ugo di Toscana, citato dallo stesso Dante Alighieri nella Divina Commedia come il “Gran Barone”. Fu lei che, nel 978, fu fondatrice del più antico monastero benedettino di Firenze, l’Abbazia di Santa Maria, meglio conosciuta come Badia Fiorentina.'
    ],
    [
      'title' => '',
      'content' => 'Il villaggio di Radda in Chianti, che divenne poco dopo capoluogo storico della Lega del Chianti, fu citato per la prima volta in un documento risalente al 1002. Attraverso questo diploma, infatti, l’Imperatore, del Sacro Romano Impero Ottone III confermava la donazione effettuata proprio dalla contessa Willa di alcuni dei territori del Chianti, tra cui Radda, in favore della Badia Fiorentina. A seguito di questo primo attestato, la località di Radda apparirà in numerosi altri documenti dell’Abbazia di Santa Maria di Firenze, fino al XII secolo. Questo a dimostrazione del grande valore strategico rappresentato dalla zona.'
    ],
    [
      'title' => '',
      'content' => 'Proprio per questo motivo, gli Agricoltori del Chianti Geografico hanno deciso di scegliere lei come volto-icona di una delle etichette più emblematiche del Chianti Classico.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
