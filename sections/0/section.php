<div class="container-fluid pr-0 pl-0">
  <div class="row homepageColumns no-gutters" style="background-image: url('<?=$imagesPath.$sections[0]['background']?>')">
    <?php foreach($sections as $section){
      if($section['index'] > 0){
        ?>
        <div class="col homepageColumn" onclick="fullpage_api.moveTo(<?=($section['index']+1)?>, 0);">
          <div class="col-content d-flex justify-content-center align-items-center" data-background="<?=$section['background']?>">
            <div class="logo">
               <!--<img src="<?=$imagesPath?>geografico-logo.png">-->
            </div>
            <div class=" text-center">
              <h1><?=$section['title']?></h1>
              <h2><?=$section['subtitle']?></h2>
              <p> <a href="#sectionAnchor<?=($section['index']+1)?>">Scopri <i class="fa fa-angle-right"></i></a></p>
              <div style="height:50px;"></div>
            <?php
              if($section['index'] == 3){
                  include 'commons/arrowScrollDown.php';
              }else{
                  include 'commons/arrowScrollDownFake.php';
              }


             ?>
            </div>
            <div class="sectionCursor">0<?=$section['index']?></div>
          </div>
        </div>
      <?php }}?>
    </div>
  </div>
