<?php

$page = [
  'superTitle' => 'La storia',
  'title' => 'La timeline degli<br>investimenti',
  'subtitle' => 'Un universo di opportunità',
  'col-width' => 5,
  'description' => [
    [
      'title' => '',
      'content' => '
        <div class="row" style="background-image:url(\'sections/5/slides/2/images/investments-plan.png\'); background-position:bottom right;  background-repeat: no-repeat; background-size: contain;">
          <div class="col-md-6" style="height:480px; opacity: 0">Il Gruppo Banca IFIS è il più grande operatore indipendente in Italia nel mercato dello specialty finance ed è presente nel settore del credito commerciale a breve, medio e lungo termine e servizi di leasing, in quello dell’acquisizione/dismissione e gestione dei portafogli di crediti non-performing e in quello dei crediti fiscali.</div>
        </div>
      '
    ]
  ],
  'photos' => [
    'handshake.png',
  ]
];

?>
<?php
$showArrowsAndLogo = false;
  $hideStdPageSubIcons = true;
 include 'commons/stdPage.php'; ?>
