<?php

$page = [
  'superTitle' => 'Partnership',
  'title' => 'LA PARTNERSHIP TRA BANCA IFIS E GEOGRAFICO',
  'subtitle' => 'Banca IFIS e Tenute Piccini hanno definito un rapporto di collaborazione che consente di migliorare i pagamenti dei propri fornitori di uva',
  'description' => [
    [
      'title' => '',
      'content' => 'Banca IFIS Impresa è la divisione del Gruppo Banca IFIS specializzata nel finanziamento alle PMI. Banca IFIS Impresa ha definito con Tenute Piccini un nuovo rapporto per la gestione dei pagamenti ai propri conferitori fornitori per la consegna dell’uva. L’operazione prevede: la cessione massiva (totale) delle fatture di acconto e saldo derivanti dalla consegna dell’uva al termine della stagione;
      il pagamento delle fatture da parte di Banca IFIS  di acconto/saldo al 100% alle scadenze concordate;'
    ],
    [
      'title' => '',
      'content' => '<span style="display:inline-block; margin-right: 5px; margin-left: -35px;"><img src="assets/images/title-pin.png"></span><strong>I vantaggi della collaborazione tra Banca IFIS Impresa e Tenute Piccini impattano tutta la filiera, rendendo immediati e definiti gli incassi dei fornitori e adeguando i tempi di pagamento della cantina al suo ciclo produttivo.</strong>'
    ],
    [
      'title' => '',
      'content' => '<strong>La collaborazione tra Tenute Piccini e Banca IFIS Impresa crea benefici per tutta la filiera:</storng><br>
      <span style="display:inline-block; margin-right: 5px; margin-left: -35px;"><img src="assets/images/title-pin.png"></span><strong>Sicurezza di incasso da parte dei fornitori</strong><br>
      <span style="display:inline-block; margin-right: 5px; margin-left: -35px;"><img src="assets/images/title-pin.png"></span><strong>Scadenze di pagamento definite</strong><br>
      <span style="display:inline-block; margin-right: 5px; margin-left: -35px;"><img src="assets/images/title-pin.png"></span><strong>Tempi di incasso per i fornitori ridotti rispetto al passato,<br>30% disponibile già a 30 giorni dalla consegna</strong><br>'
    ]
  ],
  'photos' => [
    'handshake2.png',
  ]
];

?>
<?php
$showArrowsAndLogo = false;
$hideStdPageSubIcons = true;
include 'commons/stdPage.php'; ?>
