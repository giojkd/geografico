<?php

$page = [
  'superTitle' => 'NUOVO PROGETTO ENOLOGICO',
  'title' => 'LA VALORIZZAZIONE DELLE DENOMINAZIONI STORICHE',
  'subtitle' => 'A spalleggiare la Famiglia Piccini in questo importante progetto è stato scelto il noto enologo consulente Riccardo Cotarella, affiancato nell’impresa dal giovane e talentuoso enologo toscano Alessandro Barabesi, alla guida del team tecnico della cantina.',
  'description' => [
    [
      'title' => '',
      'content' => ''
    ],
    [
      'title' => '',
      'content' => ' Il percorso di valorizzazione del territorio viene portato avanti attraverso questa collaborazione e si concentra in particolar modo sulle storiche denominazioni del senese come Chianti Classico, Vernaccia di San Gimignano e Chianti Colli Senesi, vini di grande fama che vengono affiancati ai più recenti Super Tuscan.'
    ],
    [
      'title' => '',
      'content' => '“Sono molto felice di poter dare un contributo utile alla Famiglia Piccini. Il Geografico non rappresenta per me una sfida perché i valori che porta con sé sono già una garanzia. Il nostro compito è quello di dare un ulteriore impulso alla straordinaria vocazionalità di questa azienda e dei terroir che rappresenta.”<br> <span style="display: inline-block; margin-top: 5px; font-weight: 700">Riccardo Cotarella</span>'
    ],
    [
      'title' => '',
      'content' => '
“Quella del Geografico è una realtà storica di territorio che rappresenta un punto di riferimento e d’incontro per molte generazioni di vignaioli non solo del Chianti Classico, ma dell’intera Toscana. Per questo motivo è necessario avere grande rispetto nell’approcciarla. Il mio lavoro si basa sulla riscoperta di tradizioni e sapori antichi legata ad un’applicazione tecnica moderna, in grado di soddisfare i dettami del mondo vitivinicolo odierno. Cerchiamo di portare nei nostri vini tutti i nostri valori: storia, rispetto e qualità di prodotto”<br> <span style="display: inline-block; margin-top: 5px; font-weight: 700">Alessandro Barabesi</span>'

    ]
  ],
  'photos' => [
    'handshake2.png',
  ]
];

?>
<?php
$showArrowsAndLogo = false;
$hideStdPageSubIcons = true;
include 'commons/stdPage.php'; ?>
