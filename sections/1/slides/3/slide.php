<?php
shuffle($gallery);
$page = [
  'superTitle' => 'Territorio',
  'title' => 'LE NOSTRE COLLINE, IL NOSTRO VANTO',
  'subtitle' => 'Questa è una storia che parla di vino, di territorio, di identità e di solidarietà',
  'description' => [
    [
      'title' => '',
      'content' => 'I vigneti dei centosettanta soci del Geografico sono dislocati nelle zone di produzione più prestigiose della Toscana, interessando una superficie pari a circa 550 ettari. Inoltre, più di 400 di questi ettari sono iscritti in Albi di produzioni a Denominazione di Origine. Le aree principali di produzione: Chianti Classico, Chianti Colli Senesi e Vernaccia di San Gimignano.'
    ],
    [
      'title' => 'Chianti Classico',
      'content' => 'Nella parte a sud del territorio del Chianti Classico, in particolare nei comuni storici di Castellina in Chianti, Gaiole in Chianti, Radda in Chianti e Castelnuovo Berardenga, si trovano 160 ettari di vitigni, collocati ad altitudini variabili tra i 280 ed i 600 metri s.l.m., destinati alla produzione di Chianti Classico DOCG.'
    ],
    [
      'title' => 'Chianti Colli Senesi e Vernaccia Di San Gimignano',
      'content' => 'Sempre nella provincia di Siena, a 250 metri s.l.m., si trovano 165 ettari votati alla produzione di Chianti Colli Senesi DOCG. Ne completano il quadro i circa 60 ettari radicati attorno alla bella cittadina di San Gimignano, destinati alla vinificazione dell’omonima Vernaccia DOCG.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
