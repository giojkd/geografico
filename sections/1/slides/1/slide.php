<?php
$itmelineDates = [
  [
    'title'=>'Il sangiovese',
    'year' => '1500',
    'description' => "L’origine di questo particolare vitigno sia, con tutta probabilità, etrusca (zona a nord del Tevere e a sud dell'Arno)"
  ],
  [
    'title'=>'Fattoria di Acquisto',
    'year' => '1961',
    'description' => "Diciassette viticoltori lungimiranti, - “Quelli del Geografico” - capirono l’importanza dello straordinario rapporto tra un vino e il suo territorio di produzione."
  ],
  [
    'title'=>'Fattoria di Acquisto',
    'year' => '1989',
    'description' => "Acquisto della fattoria di San Gimignano. La struttura ha una capacità di vinificazione pari a 10.000 hl in moderni tini di acciaio inox a temperatura controllata, ideali per conservazione degli aromi tipici della Vernaccia."
  ],
  [
    'title'=>'Fattoria di Acquisto',
    'year' => '1999',
    'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras faucibus quam ut lectus porttitor, ut ultricies libero cursus. "
  ],
  [
    'title'=>'Fattoria di Acquisto',
    'year' => '2009',
    'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras faucibus quam ut lectus porttitor, ut ultricies libero cursus. "
  ]
];

?>

  <div class="row no-gutters">
    <div class="col-md-5 offset-md-2 d-flex justify-content-center align-content-center">
      <div class=" d-flex justify-content-center align-items-center">
        <div class="text-center">
          <h1>Timeline</h1>

          <div class="mh-80vh">
              <img style="max-height: 70vh!important" class="img-fluid " src="<?=$imagesPath?>timeline.png" alt="">
          </div>

          <?php /*
          <div class="timeline">
            <?php foreach ($itmelineDates as $index => $date) {
              if($index%2 == 0){
                ?>
                <div class="row">
                  <div class="col text-right pr-4">
                    <div class="timelineDate">
                      <h2><?=$date['title']?></h2>
                      <h3><?=$date['year']?></h3>
                      <p><?=$date['description']?></p>
                    </div>
                  </div>
                  <div class="col"></div>
                </div>
                <?php
              }else{ ?>
                <div class="row">
                  <div class="col"></div>
                  <div class="col text-left pl-4">
                    <div class="timelineDate">
                      <h2><?=$date['title']?></h2>
                      <h3><?=$date['year']?></h3>
                      <p><?=$date['description']?></p>
                    </div>
                  </div>
                </div>
              <?php  }
            } ?>
          </div>
          */?>
        </div>
      </div>

    </div>
    <div class="col-md-5" >
      <div class="vh-100 vw-100p">
        <img class="timelineMap" src="<?=$imagesPath?>Toscana.png" alt="" style="">
        <div  style="position: absolute; bottom: 40px; right: 40px;">
          <?php include 'commons/stdPageSubIcons.php'; ?>
        </div>

      </div>
    </div>
  </div>
