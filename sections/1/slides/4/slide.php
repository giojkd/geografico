<?php
shuffle($gallery);
$page = [
  'superTitle' => 'Mission & Vision',
  'title' => 'Custodi della tradizione',
  'subtitle' => 'Alla base della filosofia del Geografico pochi, semplici ma chiari concetti: le persone, il territorio e l’identità più vera del suo vino.',
  'description' => [
    [
      'title' => 'Le Persone',
      'content' => 'Nonostante tutti i cambiamenti intercorsi nel tempo, la linea guida è rimasta sempre la stessa: l’attenzione alle persone. Per noi è fondamentale garantire ad ogni singolo coltivatore un punto di riferimento sicuro, offrendo supporto agronomico, enologico, produttivo, logistico e finanziario.'
    ],
    [
      'title' => 'Il territorio e la sua valorizzazione',
      'content' => 'La terra del Geografico non è solo materia prima e territorialità, intesa come un insieme di luoghi. Questa terra è la protagonista assoluta della nostra identità. Essa ci lega, ci identifica e ci rende orgogliosi di essere Agricoltori del Chianti Geografico. Per tutte queste ragioni, essa viene difesa e, insieme a lei, ne difendiamo il legame con il nostro prodotto storico: il suo vino.'
    ],
    [
      'title' => 'L\'identità del vino',
      'content' => 'Geografico è amore per il vino, per le persone e per il territorio locale. Proprio dal connubio di questi elementi, nasce l’identità stessa del nostro vino.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
