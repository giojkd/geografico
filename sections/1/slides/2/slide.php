<?php
$images = 31;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical/Img_'.$i.'.jpg';
}
shuffle($gallery);
$page = [
  'superTitle' => 'Storia',
  'title' => 'IL GEOGRAFICO, LA STORIA DEL CHIANTI',
  'subtitle' => 'Questa è una storia che parla di vino, di territorio, di identità e di solidarietà',
  'description' => [
    [
      'title' => '',
      'content' => 'Nella straordinaria storia del Chianti vi sono alcuni protagonisti  che più di altri hanno avuto il merito e la responsabilità di rendere leggendario questo vino, anche attraverso una sincera valorizzazione del suo territorio di produzione, unico per una grande varietà di aspetti.'
    ],
    [
      'title' => '',
      'content' => 'Oggi, dire Chianti nel mondo, significa evocare molto di più di un vino. Questo è stato reso possibile anche grazie a diciassette viticoltori lungimiranti, - “Quelli del Geografico” - che già nel 1961 capirono l’importanza dello straordinario rapporto tra un vino e il suo territorio di produzione.'
    ],
    [
      'title' => '',
      'content' => 'Proprio qui, più di mezzo secolo fa, nacque la storia del Geografico, che oggi prosegue il suo cammino grazie alla passione per il territorio della famiglia Piccini, che lo ha acquistato per far proseguire la sua memoria.'
    ],
    [
      'title' => '',
      'content' => 'Da quei diciassette viticoltori iniziali, oggi ne possiamo contare ben sessannta. Tutti prevalentemente radicati nei territori del Chianti, Chianti Classico, ma anche della Vernaccia di San Gimignano. Sessanta viticoltori appassionati che lavorano ogni giorno per valorizzare una grande denominazione e i suoi vini, a partire dal suo vitigno principe: il Sangiovese.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
