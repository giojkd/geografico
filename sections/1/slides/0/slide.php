
<div class="position-relative h-100 w-100">

  <video width="100%" height="100%" controls="" poster="<?=$siteUrl.$imagesPath?>video-poster.png" type="video/mp4" id="geograficoVideo">
    <source src="assets/videos/video-geografico.mp4" >
      Your browser does not support the video tag.
    </video>

    <div style="position: absolute; z-index: 99999; left: 50%; margin-left: -20px; bottom: 25%;">
      <div class="arrowScrollDown black">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
