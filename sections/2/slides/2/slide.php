<?php
$images = 31;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical/Img_'.$i.'.jpg';
}
shuffle($gallery);
$page = [
  'superTitle' => 'Gaiole in Chianti',
  'title' => 'La culla dei<br>nostri vini',
  'title' => 'Le eccellenze<br>del chianti classico',
  'subtitle' => 'LA PRESTIGIOSA CANTINA DI GAIOLE IN CHIANTI, IMMERSA NEL VERDE DELLE COLLINE TOSCANE',
  'subtitle' => '',
  'description' => [
    [
      'title' => '',
      'content' => 'La storica cantina di Gaiole in Chianti, cuore del Geografico, concepita con tecnologie moderne, è dotata di recipienti di acciaio inox provvisti di sistema automatico per il controllo delle temperature di fermentazione. La cantina di invecchiamento ha una capacità complessiva di 7.000 hl, suddivisi fra botti di rovere di media capienza e barriques e dispone inoltre di una moderna linea di imbottigliamento e confezionamento.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
