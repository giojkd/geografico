<?php
$images = 31;
$gallery = [];
for($i = 1 ; $i <= $images; $i ++){
  $gallery[] = 'gallery_vertical/Img_'.$i.'.jpg';
}
shuffle($gallery);
$page = [
  'superTitle' => 'San Gimignano',
  'title' => 'Le eccellenze del<br>Chianti Classico',
  'subtitle' => 'LA PRESTIGIOSA CANTINA DI SAN GIMIGNANO, IMMERSA NEL VERDE DELLE COLLINE TOSCANE',
  'subtitle' => '',
  'description' => [
    [
      'title' => '',
      'content' => 'Acquistata nel 1989 per la produzione di Vernaccia di San Gimignano e di Chianti Colli Senesi, ha permesso un’efficace razionalizzazione produttiva. La struttura ha una capacità di vinificazione pari a 10.000 hl che avviene con l’utilizzo di moderni tini di acciaio inox a temperatura controllata, ideali per conservazione degli aromi tipici della Vernaccia. Inoltre, la cantina dispone di un’area dedicata alla produzione ed invecchiamento del Vinsanto in storici caratelli.'
    ]
  ],
  'photos' => $gallery
];

?>
<?php include 'commons/stdPage.php'; ?>
