<?php
$gallery = [
  '1.png',
  '1.png',
  '1.png',
];
shuffle($gallery);
?>
<div id="carousel-<?=$section['index']?>-<?=$slide['index']?>" class="carousel slide" data-ride="carousel">
  <div class="vh-100 vw-100">


  <a href="#sectionAnchor0"><img class="geograficoLogoGallerySlider" src="assets/images/chianti-geografico.png" alt=""></a>
  <div class="geograficoSignature">
    <?php   include 'commons/arrowScrollDown.php';?><br>
    Gaiole in Chianti
  </div>
  <!--<div class="carousel-indicators-custom">
    <a href="#carousel-<?=$section['index']?>-<?=$slide['index']?>" role="button" data-slide="prev" onclick="updateCustomIndicatorsCarousel('#carousel-<?=$section['index']?>-<?=$slide['index']?>',-1)">
      <i class="fas fa-angle-up"></i>
    </a>
    <ol>
      <?php foreach($gallery as $index => $photo){
        ?>
        <li onclick="$(this).siblings().removeClass('active');$(this).addClass('active')" data-target="#carousel-<?=$section['index']?>-<?=$slide['index']?>" data-slide-to="<?=$index?>" class="<?=($index == 0) ? 'active' : ''?>"><div></div></li>
      <?php }?>
    </ol>
    <a href="#carousel-<?=$section['index']?>-<?=$slide['index']?>" role="button" data-slide="next" onclick="updateCustomIndicatorsCarousel('#carousel-<?=$section['index']?>-<?=$slide['index']?>',1)">
      <i class="fas fa-angle-down"></i>
    </a>
  </div>

  <div class="carousel-inner">
    <?php foreach($gallery as $index => $photo){
      ?>
      <div class="carousel-item <?=($index == 0) ? 'active' : ''?>">
        <img src="sections/<?=$section['index']?>/slides/<?=$slide['index']?>/images/<?=$photo?>" class="d-block w-100" alt="...">
      </div>
    <?php }?>
  </div>-->
  <div class="slick-carousel">

      <?php foreach($gallery as $index => $photo){
        ?>
        <div class="vh-100 w-100" style="background-size:cover; background-position: center center; background-repeat: no-repeat; background-image:url('sections/<?=$section['index']?>/slides/<?=$slide['index']?>/images/<?=$photo?>')"></div>
      <?php }?>

    </div>
    </div>
</div>
