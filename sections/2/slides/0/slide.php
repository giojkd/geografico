<div class="row justify-content-center align-self-center fullPage">
  <div class="col-md-7  text-center p-0">
    <h1>LE NOSTRE CANTINE</h1>
    <p class="fullPageIntro">
      Nonostante la capacità di ricevere fino a 60.000 quintali di uva, grazie all’implementazione di moderne tecnologie, le storiche cantine di Gaiole in Chianti e San Gimignano
      continuano a mantenere la stessa cura e dedizione nella lavorazione delle uve affinché queste possano esprimersi al meglio. <br>
      Da più di mezzo secolo, l’obiettivo principale del Geografico resta la ricerca costante dell’eccellenza.
    </p>
    <div class="fullPageMiddleBox">
      <!--  <img src="<?= $imagesPath ?>icons8-grapes.png" alt="">
      <br>
      <p>
      scopri le nostre cantine
    </p>
    <br>-->
      <?php include 'commons/arrowScrollDown.php' ?>
    </div>
    <div class="row justify-content-center align-self-center">
      <div class="col-md-6">
        <h2>
          <a href="javascript:" onclick="fullpage_api.moveTo(3,1)">
            San<br>Gimignano
          </a>
        </h2>
      </div>
      <div class="col-md-6">
        <h2>
          <a href="javascript:" onclick="fullpage_api.moveTo(3,2)">
            Gaiole<br>in Chianti
          </a>
        </h2>
      </div>
    </div>
    <div class="hr-white"></div>
    <div class="row justify-content-center align-self-center slide20">
      <div class="col-md-6 text-center p-0">
        <div class="d-inline-block text-left">
          <?php
          $spsl['gallery'] = $galleries['san_gimignano'];
          include 'commons/stdPageSubIcons.php'; ?>
        </div>

      </div>
      <div class="col-md-6 text-center">
        <div class="d-inline-block text-left">
          <?php
          $spsl['gallery'] = $galleries['gaiole_in_chianti'];
          include 'commons/stdPageSubIcons.php'; ?>
        </div>

      </div>
    </div>
  </div>
</div>